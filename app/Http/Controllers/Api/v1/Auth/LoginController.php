<?php
namespace App\Http\Controllers\Api\v1\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __invoke(LoginFormRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
        
        return response()->json([
            'token' => $user->createToken('browser')->plainTextToken,
            'expiration' => 1000 * 60 * 60 * 2
        ], 201);
    }
}