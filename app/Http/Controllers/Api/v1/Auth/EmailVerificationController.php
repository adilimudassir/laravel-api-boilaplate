<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\ApiCode;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;

class EmailVerificationController extends Controller
{

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        if (!hash_equals((string) $request->route('id'), (string) auth()->user()->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string) $request->route('hash'), sha1(auth()->user()->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if (auth()->user()->hasVerifiedEmail()) {
            return new JsonResponse([
                "message" => "Email already verified"
            ], ApiCode::EMAIL_ALREADY_VERIFIED);
        }

        if (auth()->user()->markEmailAsVerified()) {
            event(new Verified(auth()->user()));
        }

        return new JsonResponse(['message' => 'Email Verified Successfully', ApiCode::EMAIL_VERIFIED]);
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */

    public function resend(Request $request)
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return new JsonResponse([
                "message" => "Email already verified."
            ], ApiCode::EMAIL_ALREADY_VERIFIED);
        }

        auth()->user()->sendEmailVerificationNotification();

        return new JsonResponse([
            "message" => "Verification link sent to your Email-Address"
        ], ApiCode::EMAIL_VERIFICATION_LINK_SENT);
    }
}
