<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\PasswordResetFormRequest;
use App\Http\Requests\ResetPasswordFormRequest;

class PasswordResetController extends Controller
{
    public function sendResetLink(PasswordResetFormRequest $request)
    {
        $response = Password::broker()->sendResetLink($request->only('email'));

        if ($response == Password::RESET_LINK_SENT) {
            return new JsonResponse(['message' => 'We have emailed your password reset link!'], 200);
        }

        throw ValidationException::withMessages([
            'email' => ["We can't find a user with that email address."],
        ]);
    }

    public function reset(ResetPasswordFormRequest $request)
    {
        $response = Password::broker()->reset(
            $request->only(
                'email',
                'password',
                'password_confirmation',
                'token'
            ),
            function ($user, $password) {
                $user->password = Hash::make($password);

                $user->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );
        
        if ($response == Password::PASSWORD_RESET) {
            return new JsonResponse(['message' => 'Your password has been reset!'], 200);
        }

        throw ValidationException::withMessages([
            'token' => ["This password reset token is invalid."],
        ]);
    }
}
