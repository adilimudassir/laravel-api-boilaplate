<?php
namespace App\Http\Controllers\Api\v1\Auth;

use App\Models\User;
use App\Events\UserCreated;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterFormRequest;

class RegisterController extends Controller
{
    public function __invoke(RegisterFormRequest $request)
    {
        DB::transaction(function () use ($request) {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);

            event(new UserCreated($user));

            return $user;
        });;

        return response()->json('User Registered', 201);
    }
}