<?php

namespace App\Http\Controllers\Api\v1;

use App\Repositories\UserRepository;
use App\Http\Requests\UserPasswordFormRequest;

class UserPasswordController
{
    /**
     *  @var [UserRepository]
     */
    private UserRepository $userRepository;

    /**
     * create an instance of the controller.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function update(UserPasswordFormRequest $request, $id = null)
    {
        return $this->userRepository->updatePassword(
            $this->userRepository->getById($id ?? auth()->user()->id),
            $request->all()
        );
    }
}
