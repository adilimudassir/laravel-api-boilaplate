<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentFormRequest;
use App\Repositories\DepartmentRepository;

class DepartmentController extends Controller
{
    /**
         * Undocumented variable.
         *
         * @var [DepartmentRepository]
         */
        protected $departmentRepository;

        /**
         * create an instance of the controller.
         *
         * @param DepartmentRepository $departmentRepository
         */

    public function __construct(DepartmentRepository $departmentRepository)
    {
        $this->departmentRepository = $departmentRepository;
    }

    public function index()
    {
        $this->authorize('read-departments');

        return $this->departmentRepository->with('programmes', 'faculty')->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DepartmentFormRequest $request)
       {
           $this->authorize('create-departments');

           return $this->departmentRepository->create($request);
       }

    public function find($id)
       {
           $this->authorize('read-departments');

           return $this->departmentRepository->getById($id);
       }

   public function update(DepartmentFormRequest $request, $id)
   {
       $this->authorize('update-departments');

       return $this->departmentRepository->update(
           $request,
           $this->departmentRepository->getById($id)
       );
   }

   public function destroy($id)
   {
       $this->authorize('delete-departments');

       return $this->departmentRepository->delete($id);
   }
}
