<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FeeFormRequest;
use App\Repositories\FeeRepository;


class FeeController extends Controller
{
    /**
         * Undocumented variable.
         *
         * @var [FacultyRepository]
         */
        protected $feeRepository;

        /**
         * create an instance of the controller.
         *
         * @param FacultyRepository $departmentRepository
         */
     public function __construct(FeeRepository $feeRepository)
     {
         $this->feeRepository = $feeRepository;
     }

     public function index()
     {
         $this->authorize('read-fees');

         return $this->feeRepository->all();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create(FeeFormRequest $request)
        {
            $this->authorize('create-fees');

            return $this->feeRepository->create($request);
        }


     public function update(FeeFormRequest $request, $id)
     {
         $this->authorize('update-fees');

         return $this->feeRepository->update(
             $request,
             $this->feeRepository->getById($id)
         );
     }

     public function destroy($id)
     {
         $this->authorize('delete-fees');

         return $this->feeRepository->delete($id);
     }
}
