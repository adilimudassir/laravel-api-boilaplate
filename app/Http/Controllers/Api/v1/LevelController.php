<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LevelRepository;
use App\Http\Requests\LevelFormRequest;
class LevelController extends Controller
{
    /**
         * Undocumented variable.
         *
         * @var [FacultyRepository]
         */
        protected $levelRepository;

        /**
         * create an instance of the controller.
         *
         * @param FacultyRepository $departmentRepository
         */
     public function __construct(LevelRepository $levelRepository)
     {
         $this->levelRepository = $levelRepository;
     }

     public function index()
     {
         $this->authorize('read-levels');

         return $this->levelRepository->all();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create(LevelFormRequest $request)
        {
            $this->authorize('create-levels');

            return $this->levelRepository->create($request);
        }


     public function update(LevelFormRequest $request, $id)
     {
         $this->authorize('update-levels');

         return $this->levelRepository->update(
             $request,
             $this->levelRepository->getById($id)
         );
     }

     public function destroy($id)
     {
         $this->authorize('delete-levels');

         return $this->levelRepository->delete($id);
     }
}
