<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Permission;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function __invoke()
    {
        return Permission::all();
    }
}
