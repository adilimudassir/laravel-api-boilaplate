<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\FacultyRepository;
use App\Http\Requests\FacultyFormRequest;

class FacultyController extends Controller
{
    /**
         * Undocumented variable.
         *
         * @var [FacultyRepository]
         */
        protected $facultyRepository;

        /**
         * create an instance of the controller.
         *
         * @param FacultyRepository $departmentRepository
         */
     public function __construct(FacultyRepository $facultyRepository)
     {
         $this->facultyRepository = $facultyRepository;
     }

     public function index()
     {
         $this->authorize('read-faculties');

         return $this->facultyRepository->with('departments')->all();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create(FacultyFormRequest $request)
        {
            $this->authorize('create-faculties');

            return $this->facultyRepository->create($request);
        }


     public function update(FacultyFormRequest $request, $id)
     {
         $this->authorize('update-faculties');

         return $this->facultyRepository->update(
             $request,
             $this->facultyRepository->getById($id)
         );
     }

     public function destroy($id)
     {
         $this->authorize('delete-faculties');

         return $this->facultyRepository->delete($id);
     }
}
