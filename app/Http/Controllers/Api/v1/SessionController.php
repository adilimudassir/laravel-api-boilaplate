<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SessionRepository;
use App\Http\Requests\SessionFormRequest;

class SessionController extends Controller
{
    /**
         * Undocumented variable.
         *
         * @var [FacultyRepository]
         */
        protected $sessionRepository;

        /**
         * create an instance of the controller.
         *
         * @param FacultyRepository $departmentRepository
         */
     public function __construct(SessionRepository $sessionRepository)
     {
         $this->sessionRepository = $sessionRepository;
     }

     public function index()
     {
         $this->authorize('read-sessions');

         return $this->sessionRepository->all();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create(SessionFormRequest $request)
        {
            $this->authorize('create-sessions');

            return $this->sessionRepository->create($request);
        }


     public function update(SessionFormRequest $request, $id)
     {
         $this->authorize('update-sessions');

         return $this->sessionRepository->update(
             $request,
             $this->sessionRepository->getById($id)
         );
     }

     public function destroy($id)
     {
         $this->authorize('delete-sessions');

         return $this->sessionRepository->delete($id);
     }
}
