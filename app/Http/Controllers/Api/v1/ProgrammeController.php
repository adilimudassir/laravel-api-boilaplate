<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProgrammeFormRequest;
use App\Repositories\ProgrammeRepository;

class ProgrammeController extends Controller
{
     /**
          * Undocumented variable.
          *
          * @var [ProgrammeRepository]
          */
         protected $programmeRepository;

         /**
          * create an instance of the controller.
          *
          * @param ProgrammeRepository $programmeRepository
          */

     public function __construct(ProgrammeRepository $programmeRepository)
     {
         $this->programmeRepository = $programmeRepository;
     }

     public function index()
     {
         $this->authorize('read-programmes');

         return $this->programmeRepository->all();
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create(ProgrammeFormRequest $request)
        {
            $this->authorize('create-programmes');

            return $this->programmeRepository->create($request);
        }

     public function find($id)
        {
            $this->authorize('read-programmes');

            return $this->programmeRepository->getById($id);
        }

    public function update(ProgrammeFormRequest $request, $id)
    {
        $this->authorize('update-programmes');

        return $this->programmeRepository->update(
            $request,
            $this->programmeRepository->getById($id)
        );
    }

    public function destroy($id)
    {
        $this->authorize('delete-programmes');

        return $this->programmeRepository->delete($id);
    }
}
