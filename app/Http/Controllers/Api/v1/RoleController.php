<?php
namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RoleRepository;
use App\Http\Requests\RoleFormRequest;

class RoleController extends Controller
{
    /**
     * Undocumented variable.
     *
     * @var [RoleRepository]
     */
    protected $roleRepository;

    /**
     * create an instance of the controller.
     *
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function index(Request $request)
    {
        $this->authorize('read-roles');

        return $this->roleRepository->getAllWithPermissions();
    }

    public function create(RoleFormRequest $request)
    {
        $this->authorize('create-roles');

        return $this->roleRepository->create($request);
    }

    public function find($id)
    {
        $this->authorize('read-roles');

        return $this->roleRepository->getById($id);
    }

    public function update(RoleFormRequest $request, $id)
    {
        $this->authorize('update-roles');

        return $this->roleRepository->update(
            $request,
            $this->roleRepository->getById($id)
        );
    } 

    public function destroy($id)
    {
        $this->authorize('delete-roles');

        return $this->roleRepository->deleteById($id);
    }
}