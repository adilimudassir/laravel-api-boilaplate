<?php
namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Http\Requests\UserFormRequest;

class UserController extends Controller
{
    /**
     * Undocumented variable.
     *
     * @var [UserRepository]
     */
    protected $userRepository;

    /**
     * create an instance of the controller.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $this->authorize('read-users');

        return $this->userRepository->filter($request->all());
    }

    public function create(UserFormRequest $request)
    {
        $this->authorize('create-users');

        return $this->userRepository->create($request);
    }

    public function find($id)
    {
        $this->authorize('read-users');

        return $this->userRepository->with('roles')->getById($id);
    }

    public function update(UserFormRequest $request, $id)
    {
        $this->authorize('update-users');

        return $this->userRepository->update(
            $request,
            $this->userRepository->getById($id)
        );
    }

    public function destroy($id)
    {
        $this->authorize('delete-users');

        return $this->userRepository->delete($id);
    }
}