<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create-fees');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'session_id' => 'required|integer',
            'amount' => 'required|integer',
            'level_id' => 'required|integer',
            'description' => 'required'
        ];
    }
}
