<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgrammeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create-programmes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'code' => 'required|integer',
            'overview' => 'required',
            'academics' => 'required',
            'admission' => 'required',
            'duration' => 'required|integer',
            'department_id' => 'required|integer',
            'fee_id' => 'required|integer'
        ];
    }
}
