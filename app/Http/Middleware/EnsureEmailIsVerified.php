<?php

namespace App\Http\Middleware;

use Closure;
use App\ApiCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $redirectToRoute
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        if (! $request->user() ||
            ($request->user() instanceof MustVerifyEmail &&
            ! $request->user()->hasVerifiedEmail())) {
            return new JsonResponse(['message' => 'Email not Verified'], ApiCode::EMAIL_NOT_VERIFIED);
        }

        return $next($request);
    }
}
