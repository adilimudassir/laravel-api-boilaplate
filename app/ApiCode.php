<?php
namespace App;

class ApiCode
{
    public const EMAIL_VERIFIED = 250;
    public const EMAIL_ALREADY_VERIFIED = 251;
    public const EMAIL_NOT_VERIFIED = 252;
    public const INVALID_EMAIL_VERIFICATION_URL = 253;
    public const EMAIL_VERIFICATION_LINK_SENT = 254;
}