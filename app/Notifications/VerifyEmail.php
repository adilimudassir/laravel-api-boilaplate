<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends Notification
{
    use Queueable;

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Verify E-mail Address'))
            ->line(__('Please click the button below to verify your email address.'))
            ->action(__('Verify E-mail Address'), $this->verificationUrl($notifiable))
            ->line(__('If you did not create an account, no further action is required.'));
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        $signedUrl = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
        
        /**
         * Remove 'http://' or 'https://' from link
         */
        if (strpos($signedUrl, 'https') !== false) {
            $signedUrl = substr($signedUrl, 8);
        }

        if (strpos($signedUrl, 'http') !== false) {
            $signedUrl = substr($signedUrl, 7);
        }

        /**
         * Replace host address with origin address
         */
        $signedUrl = str_replace(
            request()->header('host'), 
            request()->header('origin'), 
            $signedUrl
        );

        return $signedUrl;
    }
}
