<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\Events\Registered;

class UserEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
    }

    public function onCreated($event)
    {
        event(new Registered($event->user));

        Log::info(
            'User Created: ' . $event->user->name .
                '. By: ' . optional(request()->user())->name ?? $event->user->name
        );
    }

    /**
     * @param [type] $event
     * @return void
     */
    public function onUpdated($event)
    {
        Log::info('User Updated: ' . $event->user->name . '. By: ' . optional(request()->user())->name ?? $event->user->name);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        // $events->listen(
        //     UserLoggedIn::class,
        //     'App\Listeners\UserEventSubscriber@onLoggedIn'
        // );

        // $events->listen(
        //     UserLoggedOut::class,
        //     'App\Listeners\UserEventSubscriber@onLoggedOut'
        // );

        $events->listen(
            UserCreated::class,
            'App\Listeners\UserEventSubscriber@onCreated'
        );

        $events->listen(
            UserUpdated::class,
            'App\Listeners\UserEventSubscriber@onUpdated'
        );
    }
}
