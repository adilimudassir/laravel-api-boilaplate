<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use EloquentFilter\Filterable;

class Level extends Model
{
    use Cachable, Filterable, HasFactory;


    protected $fillable = [
        'name',
        'programme_id'
    ];
}
