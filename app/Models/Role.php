<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Spatie\Permission\Models\Role as ModelsRole;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends ModelsRole
{
    use Cachable, Filterable, HasFactory;

    // protected $fillable = [
    //     'name',
    //     'description'
    // ];

    // public function getModelFields()
    // {
    //     return $this->fillable;
    // }
}
