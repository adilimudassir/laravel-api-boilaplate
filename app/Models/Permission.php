<?php

namespace App\Models;

use EloquentFilter\Filterable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Permission\Models\Permission as ModelsPermission;

class Permission extends ModelsPermission
{
    use Cachable, Filterable;
    protected $fillable = [
        'name',
        'description',
        'guard_name',
    ];
}
