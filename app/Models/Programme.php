<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Programme extends BaseModel
{
  use HasFactory;

    protected $fillable = [
        'title',
        'code',
        'overview',
        'academics',
        'admission',
        'duration',
        'department_id',
        'fee_id'
    ];

}
