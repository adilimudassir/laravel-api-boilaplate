<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use EloquentFilter\Filterable;

class Department extends Model
{
    use Cachable, Filterable, HasFactory;


    protected $fillable = [
        'name',
        'code',
        'faculty_id'
    ];


    public function faculty()
    {
    	return $this->belongsTo(Faculty::class);
    }


    public function programmes()
    {
    	return $this->hasMany(Programme::class);
    }
}
