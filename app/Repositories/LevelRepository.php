<?php

namespace App\Repositories;

use App\Models\Level;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\LevelFormRequest;

class LevelRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Level $level)
    {
        $this->model = $level;
    }

    public function create(LevelFormRequest $request): Level
    {
        $model = DB::transaction(function () use ($request) {
            $newLevel = $this->model::create([
                'name' => $request->name,
                'programme_id' => $request->programme_id
            ]);

            if (! $newLevel) {
                throw new GeneralException('Level could not be created at the moment');
            }

            return $newLevel;
        });

        $model->flushCache();

        return $model;
    }


    public function update(LevelFormRequest $request, Level $level): Level
    {
        $model = DB::transaction(function () use ($request, $level) {
            if (! $level->update([
                'name' => $request->name,
                'programme_id' => $request->programme_id
            ])) {
                throw new GeneralException('Level Could not be updated');
            }

            return $level;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Level
    {
        $level = $this->getById($id);

        if ($level->deleted_at !== null) {
            throw new GeneralException(__('This level is already deleted.'));
        }

        if ($this->deleteById($level->id)) {
            return $level;
        }

        throw new GeneralException('There was a problem deleting this level. Please try again.');
    }

}
