<?php

namespace App\Repositories;

use App\Models\Faculty;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\FacultyFormRequest;

class FacultyRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Faculty $faculty)
    {
        $this->model = $faculty;
    }

    public function create(FacultyFormRequest $request): Faculty
    {
        $model = DB::transaction(function () use ($request) {
            $newFaculty = $this->model::create([
                'name' => $request->name,
                'code' => $request->code
            ]);

            if (! $newFaculty) {
                throw new GeneralException('Faculty could not be created at the moment');
            }

            return $newFaculty;
        });

        $model->flushCache();

        return $model;
    }


    public function update(FacultyFormRequest $request, Faculty $faculty): Faculty
    {
        $model = DB::transaction(function () use ($request, $faculty) {
            if (! $faculty->update([
                'name' => $request->name,
                'code' => $request->code
            ])) {
                throw new GeneralException('Faculty Could not be updated');
            }

            return $faculty;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Faculty
    {
        $faculty = $this->getById($id);

        if ($faculty->deleted_at !== null) {
            throw new GeneralException(__('This faculty is already deleted.'));
        }

        if ($this->deleteById($faculty->id)) {
            return $faculty;
        }

        throw new GeneralException('There was a problem deleting this faculty. Please try again.');
    }

}
