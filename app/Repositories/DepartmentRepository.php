<?php

namespace App\Repositories;

use App\Models\Department;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\DepartmentFormRequest;

class DepartmentRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Department $department)
    {
        $this->model = $department;
    }

    public function create(DepartmentFormRequest $request): Department
    {
        $model = DB::transaction(function () use ($request) {
            $newDepartment = $this->model::create([
                'name' => $request->name,
                'code' => $request->code,
                'faculty_id' => $request->faculty_id
            ]);

            if (! $newDepartment) {
                throw new GeneralException('Department could not be created at the moment');
            }

            return $newDepartment;
        });

        $model->flushCache();

        return $model;
    }

    public function update(DepartmentFormRequest $request, Department $department): Department
    {
        $model = DB::transaction(function () use ($request, $department) {
            if (! $department->update([
                'name' => $request->name,
                'code' => $request->code,
                'faculty_id' => $request->faculty_id,
            ])) {
                throw new GeneralException('Department Could not be updated');
            }

            return $department;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Department
    {
        $department = $this->getById($id);

        if ($department->deleted_at !== null) {
            throw new GeneralException(__('This department is already deleted.'));
        }

        if ($this->deleteById($department->id)) {
            return $department;
        }

        throw new GeneralException('There was a problem deleting this department. Please try again.');
    }
}
