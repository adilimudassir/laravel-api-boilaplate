<?php

namespace App\Repositories;

use App\Models\Fee;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\FeeFormRequest;

class FeeRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Fee $fee)
    {
        $this->model = $fee;
    }

    public function create(FeeFormRequest $request): Fee
    {
        $model = DB::transaction(function () use ($request) {
            $newFee = $this->model::create([
                'session_id' => $request->session_id,
                'amount' => $request->amount,
                'level_id' => $request->level_id,
                'description' => $request->description
            ]);

            if (! $newFee) {
                throw new GeneralException('Fees could not be created at the moment');
            }

            return $newFee;
        });

        $model->flushCache();

        return $model;
    }


    public function update(FeeFormRequest $request, Fee $fee): Fee
    {
        $model = DB::transaction(function () use ($request, $fee) {
            if (! $fee->update([
                'session_id' => $request->session_id,
                'amount' => $request->amount,
                'level_id' => $request->level_id,
                'description' => $request->description
            ])) {
                throw new GeneralException('Fees Could not be updated');
            }

            return $fee;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Fee
    {
        $fee = $this->getById($id);

        if ($fee->deleted_at !== null) {
            throw new GeneralException(__('This fees is already deleted.'));
        }

        if ($this->deleteById($fee->id)) {
            return $fee;
        }

        throw new GeneralException('There was a problem deleting this fees. Please try again.');
    }

}
