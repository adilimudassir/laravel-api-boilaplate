<?php

namespace App\Repositories;

use App\Models\Session;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\SessionFormRequest;

class SessionRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Session $session)
    {
        $this->model = $session;
    }

    public function create(SessionFormRequest $request): Session
    {
        $model = DB::transaction(function () use ($request) {
            $newSession = $this->model::create([
                'name' => $request->name,
                'year' => $request->year
            ]);

            if (! $newSession) {
                throw new GeneralException('Session could not be created at the moment');
            }

            return $newSession;
        });

        $model->flushCache();

        return $model;
    }


    public function update(SessionFormRequest $request, Session $session): Session
    {
        $model = DB::transaction(function () use ($request, $session) {
            if (! $session->update([
                'name' => $request->name,
                'year' => $request->year
            ])) {
                throw new GeneralException('Session Could not be updated');
            }

            return $session;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Session
    {
        $session = $this->getById($id);

        if ($session->deleted_at !== null) {
            throw new GeneralException(__('This session is already deleted.'));
        }

        if ($this->deleteById($session->id)) {
            return $session;
        }

        throw new GeneralException('There was a problem deleting this session. Please try again.');
    }

}
