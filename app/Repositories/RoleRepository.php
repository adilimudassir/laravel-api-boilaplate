<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\RoleFormRequest;

class RoleRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    public function getAllWithPermissions()
    {
        return $this->get()->map(function ($role) {
            return [
                'id' => $role->id,
                'name' => $role->name,
                'created_at' => $role->created_at,
                'updated_at' => $role->updated_at,
                'permissions' => $role->permissions->pluck('name', 'id')->toArray(),
                'users_count' => $role->users->count(),
                'users' => $role->users->pluck('name')->toArray()
            ];
        });
    }

    public function create(RoleFormRequest $request): Role
    {
        $model = DB::transaction(function () use ($request) {
            $newRole = $this->model::create([
                'name' => $request->name,
                'guard_name' => 'web'
            ]);

            if (! $newRole) {
                throw new GeneralException('Role could not be created at the moment');
            }

            $newRole->syncPermissions($request->permissions);

            return $newRole;
        });

        $model->flushCache();

        return $model;
    }

    public function update(RoleFormRequest $request, Role $role): Role
    {
        $model = DB::transaction(function () use ($request, $role) {
            if (! $role->update([
                'name' => $request->name,
            ])) {
                throw new GeneralException('Role Could not be updated');
            }

            $role->syncPermissions($request->permissions);

            return $role;
        });

        $model->flushCache();

        return $model;
    }
}
