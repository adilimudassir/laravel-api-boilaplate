<?php

namespace App\Repositories;

use App\Models\Programme;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Http\Requests\ProgrammeFormRequest;

class ProgrammeRepository extends BaseRepository
{
    /**
     * create instance of the class.
     *
     * @param Programme $programme
     */
    public function __construct(Programme $programme)
    {
        $this->model = $programme;
    }

    public function create(ProgrammeFormRequest $request): Programme
    {
        $model = DB::transaction(function () use ($request) {
            $newProgramme = $this->model::create($this->fields($request->all()));

            if (! $newProgramme) {
                throw new GeneralException('Programme could not be created at the moment');
            }

            return $newProgramme;
        });

        $model->flushCache();

        return $model;
    }


    public function update(ProgrammeFormRequest $request, Programme $programme): Programme
    {
        $model = DB::transaction(function () use ($request, $programme) {
            if (! $programme->update($this->fields($request->all()))) {
                throw new GeneralException('Programme Could not be updated');
            }

            return $programme;
        });

        $model->flushCache();

        return $model;
    }

    /**
     * @param  int  $id
     *
     * @return User
     * @throws GeneralException
     */
    public function delete($id): Programme
    {
        $programme = $this->getById($id);

        if ($programme->deleted_at !== null) {
            throw new GeneralException(__('This programme is already deleted.'));
        }

        if ($this->deleteById($programme->id)) {
            return $programme;
        }

        throw new GeneralException('There was a problem deleting this programme. Please try again.');
    }

}
