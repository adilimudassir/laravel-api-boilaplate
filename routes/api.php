<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\FeeController;
use App\Http\Controllers\Api\v1\RoleController;
use App\Http\Controllers\Api\v1\UserController;
use App\Http\Controllers\Api\v1\LevelController;
use App\Http\Controllers\Api\v1\FacultyController;
use App\Http\Controllers\Api\v1\SessionController;
use App\Http\Controllers\Api\v1\ProgrammeController;
use App\Http\Controllers\Api\v1\Auth\LoginController;
use App\Http\Controllers\Api\v1\DepartmentController;
use App\Http\Controllers\Api\v1\PermissionController;
use App\Http\Controllers\Api\v1\Auth\LogoutController;
use App\Http\Controllers\Api\v1\UserPasswordController;
use App\Http\Controllers\Api\v1\Auth\RegisterController;
use App\Http\Controllers\Api\v1\Auth\PasswordResetController;
use App\Http\Controllers\Api\v1\Auth\EmailVerificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    
Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('email/verify/{id}/{hash}', [EmailVerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [EmailVerificationController::class, 'resend'])->name('verification.resend');

    Route::middleware(['verified'])->group(function () {
        Route::get('/', fn () => response('success', 200));
        Route::post('/logout', LogoutController::class);
        Route::get('/user', fn (Request $request) => [
            'user' => $request->user(),
            'permissions' => $request->user()->getAllPermissions()->pluck('name')->toArray(),
            'isVerified' => $request->user()->hasVerifiedEmail()
        ]);

        /**
         * User Resource
         */
        Route::prefix('users')->group(function () {
            Route::get('/', [UserController::class, 'index']);
            Route::post('/create', [UserController::class, 'create']);
            Route::get('/{id}/find', [UserController::class, 'find']);
            Route::patch('/{id}/update', [UserController::class, 'update']);
            Route::delete('/{id}/delete', [UserController::class, 'destroy']);

            Route::patch('/{id}/update-password', [UserPasswordController::class, 'update']);
        });

        /**
         * Role Resource
         */
        Route::prefix('roles')->group(function () {
            Route::get('/', [RoleController::class, 'index']);
            Route::post('/create', [RoleController::class, 'create']);
            Route::get('/{id}/find', [RoleController::class, 'find']);
            Route::patch('/{id}/update', [RoleController::class, 'update']);
            Route::delete('/{id}/delete', [RoleController::class, 'destroy']);
        });

        /**
         * Permission Resource
         */
        Route::get('/permissions', PermissionController::class);

        /**
         * Faculty Resource
         */
        Route::prefix('faculties')->group(function () {
            Route::get('/', [FacultyController::class, 'index']);
            Route::post('/create', [FacultyController::class, 'create']);
            Route::get('/{id}/find', [FacultyController::class, 'find']);
            Route::patch('/{id}/update', [FacultyController::class, 'update']);
            Route::delete('/{id}/delete', [FacultyController::class, 'destroy']);
        });
        /**
         * Department Resource
         */
        Route::prefix('departments')->group(function () {
            Route::get('/', [DepartmentController::class, 'index']);
            Route::post('/create', [DepartmentController::class, 'create']);
            Route::get('/{id}/find', [DepartmentController::class, 'find']);
            Route::patch('/{id}/update', [DepartmentController::class, 'update']);
            Route::delete('/{id}/delete', [DepartmentController::class, 'destroy']);
        });

        /**
         * Programmes Resource
         */
        Route::prefix('programmes')->group(function () {
            Route::get('/', [ProgrammeController::class, 'index']);
            Route::post('/create', [ProgrammeController::class, 'create']);
            Route::get('/{id}/find', [ProgrammeController::class, 'find']);
            Route::patch('/{id}/update', [ProgrammeController::class, 'update']);
            Route::delete('/{id}/delete', [ProgrammeController::class, 'destroy']);
        });

        /**
         * Fees Resource
         */
        Route::prefix('fees')->group(function () {
            Route::get('/', [FeeController::class, 'index']);
            Route::post('/create', [FeeController::class, 'create']);
            Route::get('/{id}/find', [FeeController::class, 'find']);
            Route::patch('/{id}/update', [FeeController::class, 'update']);
            Route::delete('/{id}/delete', [FeeController::class, 'destroy']);
        });

        /**
         * Level Resource
         */
        Route::prefix('levels')->group(function () {
            Route::get('/', [LevelController::class, 'index']);
            Route::post('/create', [LevelController::class, 'create']);
            Route::get('/{id}/find', [LevelController::class, 'find']);
            Route::patch('/{id}/update', [LevelController::class, 'update']);
            Route::delete('/{id}/delete', [LevelController::class, 'destroy']);
        });
        /**
         * Session Resource
         */
        Route::prefix('sessions')->group(function () {
            Route::get('/', [SessionController::class, 'index']);
            Route::post('/create', [SessionController::class, 'create']);
            Route::get('/{id}/find', [SessionController::class, 'find']);
            Route::patch('/{id}/update', [SessionController::class, 'update']);
            Route::delete('/{id}/delete', [SessionController::class, 'destroy']);
        });
    });
    
});


Route::middleware(['guest:sanctum'])->group(function () {
    Route::post('/login', LoginController::class);
    Route::post('/register', RegisterController::class);
    Route::post('password/email', [PasswordResetController::class, 'sendResetLink']);
    Route::post('password/reset', [PasswordResetController::class, 'reset']);
});
